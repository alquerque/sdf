from django.db import models

# Create your models here.

class LofarLTAObservation(models.Model):
    createDate = models.DateField()
    dataProductType = models.CharField(max_length=50)
    duration = models.FloatField()
    dataProductIdentifier = models.CharField(max_length=50)
    filename = models.CharField(max_length=50)

    def __str__(self):
        return self.dataProductIdentifier

MAX_STR_LENGTH = 100
MAX_STR_LENGTH_SRM = 100000
DEFAULT_INT = 0
DEFAULT_FLOAT = 0.

class UserRequests(models.Model):
    """
    Define the fields in the userrequest database
    """
    bad_baselines = models.TextField(max_length=MAX_STR_LENGTH, blank=True)
    request_datetime_utc = models.DateTimeField()
    calibrator_date = models.DateTimeField()
    calibrator_dt = models.FloatField(default=DEFAULT_FLOAT)
    calibrator_id = models.PositiveIntegerField(default=DEFAULT_INT)
    calibrator_name = models.TextField(max_length=MAX_STR_LENGTH, blank=True)
    calibrator_nchan = models.PositiveSmallIntegerField(default=DEFAULT_INT)
    calibrator_nsb = models.PositiveSmallIntegerField(default=DEFAULT_INT)
    target_date = models.DateTimeField()
    dt = models.FloatField(default=DEFAULT_FLOAT)
    field = models.TextField(max_length=MAX_STR_LENGTH, blank=True)
    target_id = models.PositiveIntegerField(default=DEFAULT_INT)
    integration = models.FloatField(default=DEFAULT_FLOAT)
    location = models.TextField(max_length=MAX_STR_LENGTH, blank=True)
    nchan = models.PositiveSmallIntegerField(default=DEFAULT_INT)
    nsb = models.PositiveSmallIntegerField(default=DEFAULT_INT)
    priority = models.PositiveSmallIntegerField(default=DEFAULT_INT)
    project_code = models.TextField(max_length=MAX_STR_LENGTH, blank=True)
    status = models.TextField(max_length=MAX_STR_LENGTH, blank=True)
    time_avg = models.PositiveSmallIntegerField(default=DEFAULT_INT)
    freq_avg = models.PositiveSmallIntegerField(default=DEFAULT_INT)
    demix_src = models.TextField(max_length=MAX_STR_LENGTH, blank=True)
    skymodel = models.TextField(max_length=MAX_STR_LENGTH, blank=True)
    cal_srm = models.TextField(max_length=MAX_STR_LENGTH_SRM, blank=True)
    target_srm = models.TextField(max_length=MAX_STR_LENGTH_SRM, blank=True)
    u_name = models.TextField(max_length=MAX_STR_LENGTH, blank=True)
    u_email = models.EmailField(max_length=MAX_STR_LENGTH, blank=True)
    raw_data = models.BooleanField(default=False)
    prefactor_ver = models.TextField(max_length=MAX_STR_LENGTH, blank=True)
