from rest_framework import serializers
from .models import LofarLTAObservation


class LofarLTASerializer(serializers.Serializer):
	class Meta:
		model = LofarLTAObservation