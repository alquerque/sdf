from django.contrib import admin
from .models import LofarLTAObservation, UserRequests
# Register your models here.

admin.site.register(LofarLTAObservation)
admin.site.register(UserRequests)
