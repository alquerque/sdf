from django.urls import path, include
from .views import observation_list, queue_status, queue_table

urlpatterns = [
    path('api/observations/', observation_list, name='observations'),
    path('api/queue/', queue_status, name='queue_status'),
    path('api/table/', queue_table, name='queue_table')
]
