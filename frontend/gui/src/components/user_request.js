import React from 'react';
import axios from 'axios';
import { FormGroup, Form, Input, Button, Col, Label } from 'reactstrap';
import { Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import Select from 'react-select';
// Get fontawesome icons for loading spinners
//import { fa, fa-spinner, fa-spin } from 

// Define the options for skymodel
const skymodel_options = [
    {value: 'TGSS', label: 'TGSS'},
    {value: 'GSM',  label: 'GSM'},
];

const LABEL_WIDTH = 4
const FIELD_WIDTH = 5

let fileCalReader;
let fileTarReader;

export default class UserRequest extends React.Component {
    // Define the constructor
    constructor(props) {
	    super(props);
	    this.state = {
            targetSRM: '',
            userName: '',
            userEmail: '',
            badBaselines: '',
            demixSrc: '',
            tAvg: '4',
            fAvg: '4',
            skymodel: skymodel_options[0],
            cygA: false,
            virA: false,
            casA: false,
            tauA: false,
            calSRMList: '',
            tarSRMList: '',
            errorModalState: false,
            errorMsg: '',
            successModalState: false,
            successMsg: '',
            loading: false
        };
	}
	
	// Close the success modal
	closeSuccessModal = successModalState => {
	    this.setState({ successModalState: false });
	    // When the user closes the success message, 
	    // we should also reset all the input fields
	    window.location.reload(false)
	}
	
	// Define toggle function for error modal
	toggleErrorModal = errorModalState => {
	    this.setState({
	        errorModalState: !this.state.errorModalState
	    });
	}
	
	// Define what happens when Reset button is clicked
	handleReset = (event) => {
	    // It is easier to just reload the page than to reset the 
	    // fields individually.
	    window.location.reload(false)
	}
	
	// Handle changes to the fields
	// Dropdown menu and filereader are handled separately below
	handleChange = (event) => {
	    const target = event.target;
	    const value = target.type === 'checkbox' ? target.checked : target.value;
	    this.setState({
	        [target.name]: value
	    });
	}
	
	// Handle changes to dropdown menu.
	handleChangeSkyModel = skymodel => {
	    this.setState({ skymodel: skymodel });
	};
	
	// When the user select a calibrator file, store its content into calSRMList
	handleCal = (file) => {
	    fileCalReader = new FileReader()
	    fileCalReader.onloadend = () => {
	        this.setState({calSRMList: fileCalReader.result})
	    }
	    fileCalReader.readAsText(file)
	};
	
	// When the user select a calibrator file, store its content into calSRMList
	handleTar = (file) => {
	    fileTarReader = new FileReader()
	    fileTarReader.onloadend = () => {
	        this.setState({tarSRMList: fileTarReader.result})
	    }
	    fileTarReader.readAsText(file)
	};

    // Define what to do when the Search button is clicked
    handleSubmit = (event) => {
        this.setState({loading: true});
        // need to parse searchString if more complicated query is allowed
        axios.put('http://127.0.0.1:8080/api/observations/',{
            params: {
                calSRMList: this.state.calSRMList,
                tarSRMList: this.state.tarSRMList,
                userName: this.state.userName,
                userEmail: this.state.userEmail,
                badBaselines: this.state.badBaselines,
                tAvg: this.state.tAvg,
                fAvg: this.state.fAvg,
                skyModel: this.state.skymodel.value,
                isCygA: this.state.cygA,
                isVirA: this.state.virA,
                isCasA: this.state.casA,
                isTauA: this.state.tauA,
            }
        })
        .then( res => {
            this.setState({loading: false});
            if(res.data.is_error === true) {
                // Show error message
                this.setState({
                    errorModalState: true,
                    errorMsg: res.data.msg
                });
            }
            else {
                // Show success message
                this.setState({
                    successModalState: true,
                    successMsg: res.data.msg
                })
                // reset all input fields
            }
        })
        .catch( error => {
            console.log(error);
        });
    }

	// Create the page layout
    render() {
        return (
            <Form>
            
            {/* Create space between the header and the body*/}
            <FormGroup row>
            </FormGroup>
            <FormGroup row>
            </FormGroup>

            {/* Calibrator SRM input*/}
            <FormGroup row>
            <Label sm={LABEL_WIDTH}>Calibrator SRM file:</Label>
            <Col sm={FIELD_WIDTH}>
                <Input 
                 type="file" 
                 id="calfile" 
                 onChange={e => this.handleCal(e.target.files[0])}
                />
            </Col>
            </FormGroup>
            
            {/* Target SRM input*/}
            <FormGroup row>
            <Label sm={LABEL_WIDTH}>Target SRM file:</Label>
            <Col sm={FIELD_WIDTH}>
                <Input 
                 type="file" 
                 id="tarfile" 
                 onChange={e => this.handleTar(e.target.files[0])}
                />
            </Col>
            </FormGroup>
            
            {/* Get user information */}
            <FormGroup row>
            <Label sm={LABEL_WIDTH}>User name:</Label>
            <Col sm={FIELD_WIDTH}>
                <Input placeholder=''
                 name="userName"
                 value={this.state.userName}
                 onChange={this.handleChange.bind(this)}
                />
            </Col>
            </FormGroup>
            <FormGroup row>
            <Label sm={LABEL_WIDTH}>Email address:</Label>
            <Col sm={FIELD_WIDTH}>
                <Input placeholder=''
                 name="userEmail"
                 value={this.state.userEmail}
                 onChange={this.handleChange.bind(this)}
                />
            </Col>
            </FormGroup>
            
            {/* Get bad baselines if any */}
            <FormGroup row>
            <Label sm={LABEL_WIDTH}>Bad baselines:</Label>
            <Col sm={FIELD_WIDTH}>
                <Input placeholder=''
                 name="badBaselines"
                 value={this.state.badBaselines}
                 onChange={this.handleChange.bind(this)}
                />
            </Col>
            </FormGroup>
            
            {/* Get sources to demix if any if any */}
            <FormGroup row>
            <Label sm={LABEL_WIDTH}>Sources to demix:</Label>
            <Col sm={FIELD_WIDTH}>
                <FormGroup check>
                <Input type="checkbox" 
                       name="cygA" 
                       onChange={this.handleChange.bind(this)}
                />Cyg A
                </FormGroup>
                <FormGroup check>
                <Input type="checkbox" 
                       name="casA"
                       onChange={this.handleChange.bind(this)}
                />Cas A
                <FormGroup check>
                </FormGroup>
                <Input type="checkbox" 
                       name="tauA"
                       onChange={this.handleChange.bind(this)}
                />Tau A
                <FormGroup check>
                </FormGroup>
                <Input type="checkbox" 
                       name="virA"
                       onChange={this.handleChange.bind(this)}
                />Vir A
                </FormGroup>
            </Col>
            </FormGroup>
            
            {/* Time and frequency averaging parameters */}
            <FormGroup row>
            <Label sm={LABEL_WIDTH}>Time averaging:</Label>
            <Col sm={FIELD_WIDTH}>
                <Input placeholder=''
                 name="tAvg"
                 value={this.state.tAvg}
                 onChange={this.handleChange.bind(this)}
                />
            </Col>
            </FormGroup>
            <FormGroup row>
            <Label sm={LABEL_WIDTH}>Frequency averaging:</Label>
            <Col sm={FIELD_WIDTH}>
                <Input placeholder=''
                 name="fAvg"
                 value={this.state.fAvg}
                 onChange={this.handleChange.bind(this)}
                />
            </Col>
            </FormGroup>
            
            {/* Source for skymodel: TGSS or GSM" */}
            <FormGroup row>
            <Label sm={LABEL_WIDTH}>Skymodel:</Label>
            <Col sm={FIELD_WIDTH}>
            <Select onChange={this.handleChangeSkyModel}
                    options={skymodel_options}
                    value={this.state.skymodel}
            />
            </Col>
            </FormGroup>
            
            {/* Submit and Reset buttons */}
            <FormGroup row>
            <Col sm={2}><Button onClick={this.handleSubmit}>Submit</Button></Col>
            <Col sm={2}><Button onClick={this.handleReset}>Reset</Button></Col>
            </FormGroup>
            
            {/* Modal to display loading message to the user */}
            <Modal isOpen={this.state.loading}>
                <ModalHeader>
                    <i className="fa fa-spinner fa-spin" /> Loading...
                </ModalHeader>
                <ModalBody>
                    We are processing your request. This can take a few minutes. Please do not close this window.
                </ModalBody>
            </Modal>
            
            {/* Modal to display error messages to user */}
            <Modal isOpen={this.state.errorModalState}>
                <ModalHeader>
                    Error
                </ModalHeader>
                <ModalBody>
                    {this.state.errorMsg}
                </ModalBody>
                <ModalFooter>
                    <Button onClick={this.toggleErrorModal}>Close</Button>
                </ModalFooter>
            </Modal>
            
            {/* Modal on success */}
            <Modal isOpen={this.state.successModalState}>
                <ModalHeader>
                    Request Submitted
                </ModalHeader>
                <ModalBody>
                    {this.state.successMsg}
                </ModalBody>
                <ModalFooter>
                    <Button onClick={this.closeSuccessModal}>Close</Button>
                </ModalFooter>
            </Modal>
            
            </Form>
		);
  }
}
