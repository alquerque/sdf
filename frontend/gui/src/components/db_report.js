import React from 'react';
import axios from 'axios';
import { Container, Row, Label } from 'reactstrap';

export default class DataBaseSummary extends React.Component {
    // Define the constructor
    constructor(props) {
        super(props);
        this.state = {
            processedEntries: '',
            totalEntries: ''
        };
    }
    
    // When the page loads, we need to populate this component
    componentDidMount = () => {
        axios.get('http://127.0.0.1:8080/api/queue/',{
        })
        .then( res => {
            this.setState({
                processedEntries: res.data.processed_queries,
                totalEntries: res.data.total_queries
            })
        })
        .catch( error => {
            console.log(error)
        });
    }

    // Create the layout
    render () {
        return (            
            <Container>
            <Row> 
               <Label>
               Processing status: {this.state.processedEntries}/{this.state.totalEntries}
                requests processed.
               </Label>
            </Row>
            </Container>
        )
    }
}
