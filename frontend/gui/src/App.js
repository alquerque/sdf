import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import './App.css';
import UserRequest from './components/user_request';
import DataBaseSummary from './components/db_report';
import DataBaseTable from './components/db_table';

function App() {


  return (
    <Container>
        <h1>ASTRON Science Delivery Framework</h1>
        <Row>
        <Col>
            <UserRequest/>
        </Col>
        <Col>
            <DataBaseSummary/>
            <DataBaseTable/>
        </Col>
        </Row>
    </Container>
  );
}

export default App;
